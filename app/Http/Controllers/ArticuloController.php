<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Articulo;

class ArticuloController extends Controller {

    public function index(Request $request) {
        //if(!$request->ajax()) return redirect( '/' );
        $criterio = $request->criterio;
        $buscar = $request->buscar;
        if($buscar == '') {
            $articulos = Articulo::join('categorias', 'articulos.id_categoria', '=', 'categorias.id')
                ->select(   'articulos.id', 'articulos.codigo', 'articulos.nombre', 
                            'categorias.nombre as nombre_categoria', 'articulos.id_categoria',
                            'articulos.precio_venta', 'articulos.stock', 'articulos.descripcion',
                            'articulos.estado')
                ->orderBy('articulos.id', 'desc')->paginate(3);                   // eloquent
        }
        else{
            $articulos = Articulo::join('categorias', 'articulos.id_categoria', '=', 'categorias.id')
                ->select(   'articulos.id', 'articulos.codigo', 'articulos.nombre', 
                            'categorias.nombre as nombre_categoria', 'articulos.id_categoria',
                            'articulos.precio_venta', 'articulos.stock', 'articulos.descripcion',
                            'articulos.estado')
                ->where('articulos.' . $criterio, 'like', '%' . $buscar . '%')
                ->orderBy('articulos.id', 'desc')->paginate(3);
        }
        return [
            'paginacion' => [
                'total' => $articulos->total(),
                'pagina_actual' => $articulos->currentPage(),
                'por_pagina' => $articulos->perPage(),
                'ultima_pagina' => $articulos->lastPage(),
                'desde' => $articulos->firstItem(),
                'hasta' => $articulos->lastItem()
            ],
            'articulos' => $articulos
        ];
    }
    public function create() {
        
    }
    public function store(Request $request) {
        $articulo = new Articulo();
        $articulo->id_categoria = $request->id_categoria;
        $articulo->codigo = $request->codigo;
        $articulo->nombre = $request->nombre;
        $articulo->precio_venta = $request->precio_venta;
        $articulo->stock = $request->stock;
        $articulo->descripcion = $request->descripcion;
        $articulo->estado = '1';
        $articulo->save();
    }
    
    public function show($id) {
        
    }
    public function edit($id) {
        
    }
    public function update(Request $request) {
        $articulo = Articulo::findOrFail( $request->id );
        $articulo->id_categoria = $request->id_categoria;
        $articulo->codigo = $request->codigo;
        $articulo->precio_venta = $request->precio_venta;
        $articulo->stock = $request->stock;
        $articulo->descripcion = $request->descripcion;
        $articulo->estado = '1';
        $articulo->save();
    }

    public function darBajaEstado(Request $request) {
        $articulo = Articulo::findOrFail( $request->id );
        $articulo->estado = '0';
        $articulo->save();
    }

    public function darAltaEstado(Request $request) {
        $articulo = Articulo::findOrFail( $request->id );
        $articulo->estado = '1';
        $articulo->save();
    }

    public function destroy($id) {
        
    }

    public function buscarArticulo(Request $request){
        $filtro = $request->filtro;
        $articulos = Articulo::where('id', '=', $filtro)
            ->select('id', 'nombre')->orderBy('nombre', 'asc')
            ->take(1)->get();
        return ['articulos' => $articulos];
    }

    public function buscarArticuloVenta(Request $request){
        $criterio = $request->criterio;
        $buscar = $request->buscar;
        if($buscar == '') {
            $articulos = Articulo::join('categorias', 'articulos.id_categoria','=','categorias.id')
                ->select(   'articulos.id', 'articulos.nombre','articulos.id_categoria',
                            'categorias.nombre as categoria','articulos.codigo',
                            'articulos.precio_venta','articulos.stock','articulos.estado')
                ->where('articulos.stock', '>', 0)
                ->orderBy('articulos.id', 'desc')
                ->paginate(10);
        }
        else{
            $articulos = Articulo::join('categorias', 'articulos.id_categoria','=','categorias.id')
                ->select(   'articulos.id', 'articulos.nombre','articulos.id_categoria',
                            'categorias.nombre as categoria','articulos.codigo',
                            'articulos.precio_venta','articulos.stock','articulos.estado')
                ->where('articulos.'.$criterio, 'like', '%'.$buscar.'%')
                ->where('articulos.stock', '>', 0)
                ->orderBy('articulos.id', 'desc')
                ->paginate(10);
        }
        return [
            'articulos' => $articulos
        ];
    }

    public function buscarArticuloConStock(Request $request){
        $filtro = $request->filtro;
        $articulos = Articulo::where('id', '=', $filtro)
            ->select('id', 'nombre', 'stock', 'precio_venta')
            ->where('stock', '>', 0)
            ->orderBy('nombre', 'asc')
            ->take(1)->get();
        return ['articulos' => $articulos];
    }

    public function getArticulos(Request $request) {
        //if(!$request->ajax()) return redirect( '/' );
        $criterio = $request->criterio;
        $buscar = $request->buscar;
        if($buscar == '') {
            $articulos = Articulo::join('categorias', 'articulos.id_categoria', '=', 'categorias.id')
                ->select(   'articulos.id', 'articulos.codigo', 'articulos.nombre', 
                            'articulos.precio_venta', 'articulos.stock',
                            'articulos.estado')
                ->orderBy('articulos.id', 'desc')->paginate(10);
        }
        else{
            $articulos = Articulo::join('categorias', 'articulos.id_categoria', '=', 'categorias.id')
                ->select(   'articulos.id', 'articulos.codigo', 'articulos.nombre', 
                            'articulos.precio_venta', 'articulos.stock',
                            'articulos.estado')
                ->where('articulos.' . $criterio, 'like', '%' . $buscar . '%')
                ->orderBy('articulos.id', 'desc')->paginate(10);
        }
        return [ 'articulos' => $articulos ];
    }

    public function getArticulosPDF(){
        $articulos = Articulo::join('categorias', 'articulos.id_categoria', '=', 'categorias.id')
            ->select(   'articulos.id', 'articulos.codigo', 'articulos.nombre', 
                        'categorias.nombre as nombre_categoria', 'articulos.id_categoria',
                        'articulos.precio_venta', 'articulos.stock', 'articulos.descripcion',
                        'articulos.estado')
            ->orderBy('articulos.nombre', 'desc')->get(); 
        $cantidad = Articulo::count();
        $pdf = \PDF::loadView( 'pdf.articulos_pdf', ['articulos'=>$articulos, 'cantidad'=>$cantidad] );
        return $pdf->download( 'articulos.pdf' );
    }
    

}
