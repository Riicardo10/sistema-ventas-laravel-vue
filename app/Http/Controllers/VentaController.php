<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\DetalleVenta;
use App\Venta;
use Carbon\Carbon;
use App\User;
use App\Notifications\NotifyAdmin;

class VentaController extends Controller {
    
    public function index(Request $request) {

        //if(!$request->ajax()) return redirect( '/' );

        $criterio = $request->criterio;
        $buscar = $request->buscar;
        if($buscar == '') {
            $ventas = Venta::join('personas', 'ventas.id_cliente', '=', 'personas.id')
                ->join('users', 'ventas.id_usuario', '=', 'users.id_persona')
                ->select(   'ventas.id','ventas.tipo_comprobante','ventas.numero_comprobante',
                            'ventas.serie_comprobante','ventas.fecha_hora','ventas.impuesto',
                            'ventas.total','ventas.estado',
                            'personas.id as cliente_id', 'personas.nombre', 'users.usuario' )
                ->orderBy('ventas.id', 'desc')->paginate(3);
        }
        else{
            $ventas = Venta::join('personas', 'ventas.id_cliente', '=', 'personas.id')
                ->join('users', 'ventas.id_usuario', '=', 'users.id_persona')
                ->select(   'ventas.id','ventas.tipo_comprobante','ventas.numero_comprobante',
                            'ventas.serie_comprobante','ventas.fecha_hora','ventas.impuesto',
                            'ventas.total','ventas.estado',
                            'personas.id as cliente_id', 'personas.nombre', 'users.usuario' )
                ->where('ventas.' . $criterio, 'like', '%' . $buscar . '%')
                ->orderBy('ventas.id', 'desc')->paginate(3);
        }
        return [
            'paginacion' => [
                'total' => $ventas->total(),
                'pagina_actual' => $ventas->currentPage(),
                'por_pagina' => $ventas->perPage(),
                'ultima_pagina' => $ventas->lastPage(),
                'desde' => $ventas->firstItem(),
                'hasta' => $ventas->lastItem()
            ],
            'ventas' => $ventas
        ];
    }

    public function getCabecera(Request $request){
        $id = $request->id;
        $cabecera = Venta::join('personas', 'ventas.id_cliente', '=', 'personas.id')
            ->join('users', 'ventas.id_usuario', '=', 'users.id_persona')
            ->select(   'ventas.id','ventas.tipo_comprobante','ventas.numero_comprobante',
                        'ventas.serie_comprobante','ventas.fecha_hora','ventas.impuesto',
                        'ventas.total','ventas.estado', 'personas.nombre', 'users.usuario' )
            ->where('ventas.id','=',$id)
            ->orderBy('ventas.id', 'desc')->take(1)->get();
        return [ 'cabecera' => $cabecera ];
    }

    public function getDetalles(Request $request){
        $id = $request->id;
        $detalles = DetalleVenta::join('articulos', 'detalle_ventas.id_articulo', '=', 'articulos.id')
            ->select(   'detalle_ventas.id', 'detalle_ventas.cantidad','detalle_ventas.precio', 
            'detalle_ventas.descuento', 'articulos.id', 'articulos.nombre', 'articulos.codigo' )
            ->where('detalle_ventas.id_venta','=',$id)
            ->orderBy('detalle_ventas.id', 'desc')->get();
        return [ 'detalles' => $detalles ];
    }

    public function create() {
        
    }
    public function store(Request $request) {
        try{
            DB::beginTransaction();
            $time = Carbon::now('America/Mexico_City');
            $venta = new Venta();
            $venta->id_cliente = $request->id_cliente;
            $venta->id_usuario = \Auth::user()->id_persona;
            $venta->tipo_comprobante = $request->tipo_comprobante;
            $venta->serie_comprobante = $request->serie_comprobante;
            $venta->numero_comprobante = $request->numero_comprobante;
            $venta->fecha_hora = $time->toDateTimeString();
            $venta->impuesto = $request->impuesto;
            $venta->total = $request->total;
            $venta->estado = 'Registrado';
            $venta->save();

            $detalles = $request->data;      // lista de detalles
            for($i=0; $i<count($detalles); $i++) {
                $detalle = new DetalleVenta();
                $detalle->id_venta = $venta->id;
                $detalle->id_articulo = $detalles[$i]['id_articulo'];
                $detalle->cantidad = $detalles[$i]['cantidad'];
                $detalle->precio = $detalles[$i]['precio'];
                $detalle->descuento = $detalles[$i]['descuento'];
                $detalle->save();
            }
            $fecha_actual = date( 'Y-m-d' );
            $numero_ventas = DB::table('ventas')->whereDate( 'created_at', $fecha_actual )->count();
            $numero_ingresos = DB::table('ingresos')->whereDate( 'created_at', $fecha_actual )->count();
            $datos = [
                'ventas' => [
                    'numero'=> $numero_ventas,
                    'msj'=> 'Ventas'
                ],
                'ingresos' => [
                    'numero'=> $numero_ingresos,
                    'msj'=> 'Ingresos'
                ],
            ];
            $all_users = User::all();
            foreach( $all_users as $user ) {
                User::findOrFail( $user->id_persona )->notify( new NotifyAdmin( $datos ) );
            }
            DB::commit();
            return [ 'id' => $venta->id ];
        }
        catch(Exception $e){
            DB::rollBack();
        }
    }

    public function darBajaEstado(Request $request) {
        $venta = Venta::findOrFail( $request->id );
        $venta->estado = 'Anulado';
        $venta->save();
    }

    public function show($id) {
        
    }
    public function edit($id) {
        
    }
    public function update(Request $request, $id) {
        
    }
    public function destroy($id) {
        //
    }

    public function getVentaPDF( Request $request, $id ) {
        $venta = Venta::join('personas', 'ventas.id_cliente', '=', 'personas.id')
            ->join('users', 'ventas.id_usuario', '=', 'users.id_persona')
            ->select(   'ventas.id','ventas.tipo_comprobante','ventas.numero_comprobante',
                        'ventas.serie_comprobante', 'ventas.created_at',
                        'ventas.fecha_hora','ventas.impuesto',
                        'ventas.total','ventas.estado',
                        'personas.nombre', 'personas.tipo_documento', 'personas.numero_documento',
                        'personas.direccion', 'personas.telefono', 'personas.email',
                        'users.usuario' )
            ->where('ventas.id', '=', $id)
            ->orderBy('ventas.id', 'desc')->take(1)->get();
        
        $detalles = DetalleVenta::join('articulos', 'detalle_ventas.id_articulo', '=', 'articulos.id')
            ->select(   'detalle_ventas.cantidad','detalle_ventas.precio', 
            'detalle_ventas.descuento', 'articulos.nombre', 'articulos.codigo', 'articulos.descripcion' )
            ->where('detalle_ventas.id_venta','=',$id)
            ->orderBy('detalle_ventas.id', 'desc')->get();
        
        $numero_venta = Venta::select('numero_comprobante')->where('id',$id)->get();

        //return view( 'pdf.venta_pdf', compact( ['venta','detalles'] ) );

        $pdf= \PDF::loadView( 'pdf.venta_pdf', [
            'venta'=>$venta, 
            'detalles'=>$detalles, 
            'numero_venta'=>$numero_venta
        ] );

        return $pdf->download( 'venta-' . $numero_venta[0]->numero_comprobante . '.pdf' );
    }

}
